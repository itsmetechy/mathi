module.exports = {
    site: {
      title: 'Data fetching',
      description: 'A boilerplate for a simple web application with a Node.JS and Express backend, with an EJS template with using Twitter Bootstrap.'
  },
  updatetime: function(){
    var date = new Date();
    return date.toLocaleTimeString();
  },
  author: {
      name: 'shiv',
      contact: 'test@gmail.com'
  }

}
