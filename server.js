var express = require('express');
var app = express();
var fs = require('fs');
var path = require('path');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Connecting to local DB
mongoose.connect('mongodb://127.0.0.1:27017/Employees');

var Schema = new mongoose.Schema({
  first_name: String,
  last_name: String,
  email: String
});

var user = mongoose.model('users', Schema);


var config = require('./config');

// Setting view engine
app.set('view engine', 'ejs');

// var user = {
//    "user4" : {
//       "name" : "mohit",
//       "password" : "password4",
//       "profession" : "teacher",
//       "id": 4
//    }
// }
//
// app.get('/', (req, res) => {
//   res.send('Access this url : "listUsers"')
// });
//
// router.get('/listUsers', function(req, res) {
//   fs.readFile(__dirname+"/"+"users.json", function(err, data) {
//     console.log(data);
//     res.end(data);
//   });
// });
//
// app.post('/addUser', function (req, res) {
//    // First read existing users.
//    fs.readFile( __dirname + "/" + "users.json", function (err, data) {
//        data = JSON.parse( data );
//        data["user4"] = user["user4"];
//        console.log( data );
//        res.end( JSON.stringify(data));
//    });
// })
//
// app.get('/:id', function (req, res) {
//    // First read existing users.
//    fs.readFile( __dirname + "/" + "users.json", 'utf8', function (err, data) {
//        users = JSON.parse( data );
//        var user = users["user" + req.params.id]
//        console.log( user );
//        res.end( JSON.stringify(user));
//    });
// })

var obj = fs.readFileSync("users.json");
var userData = JSON.parse(obj);
var username = userData['user1'].name;

var newUserData = [];

for ( var i in userData){
  newUserData.push(userData[i]);
}

// console.log(newUserData);

app.get('/', function(req, res) {
  res.render('index', {
    pageTitle:  config.site.title + ' About page',
    page_name: 'about_page',

  });
});




app.get('/view', function(req, res) {

  user.find({}, function(err, docs) {
    if (err) {
      res.json(err);
    } else {
      res.render('view', {
        pageTitle:'Data Page',
        page_name: 'view_page',
        users:docs
      });
    }
  });
});

app.get('/oneuser', function(req, res) {
  user.find({first_name: 'Marthaa'}, function(err, users) {

    console.log(users);
    if(err) throw err;
    res.render('oneuser', {
      pageTitle:'About page',
      page_name: 'about_page',
      oneuser: users
    });
  })
});

app.post('/insertdata', function(req, res) {
  console.log(req.body.email);
  new user({
    email:req.body.email,
    first_name: req.body.first_name,
    last_name: req.body.last_name
  }).save(function(err, doc) {
    if(err) res.json(err);
    else res.redirect('/view');
  })
});

app.get('/about', function(req, res) {
  res.render('about', {
    pageTitle:'About page',
    page_name: 'about_page'
  });
});

app.get('/data', function(req, res) {
  // console.log(userData.length);
  res.render('data', {
    pageTitle:'Data Page',
    page_name: 'data_page',
    siteData: newUserData
  });
});



app.listen('9091', function() {
  console.log('Server running at 9091');
});
