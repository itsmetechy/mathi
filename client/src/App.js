import React, {Component} from "react";
import Student from './Student';

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      name: 'Shankar',
      val :0
    }

  }

  changeName() {
    this.setState({
      name:'Shiva'
    })
  }

  counter() {
    this.setState({
      val:this.state.val+1
    })
  }

  newText(newName) {
    this.setState({
      name: newName
    })
  }

  render() {
    return(
      <div>
        <h3>Student List Component(App Component)</h3>
        Hello {this.state.name}
        <h1 onClick={this.counter.bind(this)}>{this.state.val}</h1>
        <button onClick={this.changeName.bind(this)}>Change Name from parent component</button>
        <hr />
        <h3> Student List ( child component ) </h3>
        <Student fromChild={this.newText.bind(this)} studentName={this.state.name} />
      </div>
    )
  }
}
