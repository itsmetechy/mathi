import React, {Component} from 'react';

export default class Student extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: 'testt',
      val :0
    }
  }

changeName() {
  this.props.fromChild('Harcoded name from child')
}

  render() {
    const list = [1,2,3];
    const dataList = list.map((i,f) => {
      return <li key={i}>{f}</li>;
    });
    return (
      <div>
        <p>From child {this.props.studentName}</p>
        <button onClick={this.changeName.bind(this)}> Change Name from child component </button>
        <hr />

        <h3>Iterating through data</h3>
        <ul>
            {dataList}
        </ul>

      </div>
    );
  }
}
